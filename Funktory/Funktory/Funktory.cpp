#define _CRT_SECURE_NO_WARNINGS // Nie używamy wielu wątków, a więc możemy
#include <iostream>
#include <chrono>
#include <string>
#include <iomanip>
#include "SimpleIntervalExecutor.h"
void displayTime(std::chrono::time_point<std::chrono::system_clock> time) 
{
	std::time_t now_c = std::chrono::system_clock::to_time_t(time);
	std::cout << std::ctime(&now_c);
}
class funktor
{
	std::string nazwa;
	long long counter;
public:
	void operator() (std::chrono::time_point<std::chrono::system_clock> start)
	{
		std::cout << ("[" + (nazwa) + "]uruchomiony po raz ") << ++counter << '\n';
	}
	funktor(std::string nazwa)
		:nazwa(nazwa),
		counter(0)
	{}
};
int main()
{
	std::string ciag;
	std::cout << "Podaj ciag znakow" << std::endl;
	std::cin >> ciag;
	SimpleIntervalExecutor sim;
sim.register_f(displayTime, std::chrono::milliseconds(1000));
sim.register_f(funktor("f1"), std::chrono::milliseconds(500));
sim.register_f(funktor("f2"), std::chrono::milliseconds(1300));
sim.register_f([&ciag](std::chrono::time_point<std::chrono::system_clock> time) {std::cout << ciag << std::endl; }, std::chrono::milliseconds(4000));
sim.run();
}