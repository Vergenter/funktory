#include "SimpleIntervalExecutor.h"
#include <thread>


void SimpleIntervalExecutor::register_f(myfunc f, std::chrono::milliseconds interval)
{
	functions.push_back(FuncDataPair(f, interval));
}

void SimpleIntervalExecutor::run()
{
	std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
	std::chrono::milliseconds elapsed_time;
	for(;;)
	{
		elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()-start);
		for (auto& funcdata : functions)
		{
			if (funcdata.interval*funcdata.counter <= elapsed_time)
			{
				funcdata.func(std::chrono::system_clock::now());
				funcdata.counter++;
			}
		}
		std::chrono::milliseconds minTimeToSleep(1000);
		for (auto & funcdata : functions)
		{
			if ( minTimeToSleep > elapsed_time % funcdata.interval)
				minTimeToSleep = (elapsed_time % funcdata.interval);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(minTimeToSleep));
	}
}

SimpleIntervalExecutor::SimpleIntervalExecutor()
{
}


SimpleIntervalExecutor::~SimpleIntervalExecutor()
{
}
