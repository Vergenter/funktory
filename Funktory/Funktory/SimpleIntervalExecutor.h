#pragma once
#include <chrono>
#include <functional>
#include <vector>
typedef std::function<void(std::chrono::system_clock::time_point)> myfunc;
class FuncDataPair
{
public:
	int counter;
	std::chrono::milliseconds interval;
	myfunc func;
	FuncDataPair(myfunc f, std::chrono::milliseconds interv)
		:interval(interv),
		func(f),
		counter(0)
	{
	}
	void inc()
	{
		counter++;
	}
};
class SimpleIntervalExecutor
{
	std::vector<FuncDataPair> functions;
public:
	void register_f(myfunc,	std::chrono::milliseconds interval);
	void run();

	SimpleIntervalExecutor();
	~SimpleIntervalExecutor();
};

